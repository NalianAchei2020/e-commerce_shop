FROM node:16-alpine 

WORKDIR /client

COPY client/package.json client/package-lock.json ./

RUN npm install

COPY client/ ./


EXPOSE 3000

CMD ["npm", "start"]

